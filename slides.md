---
# try also 'default' to start simple
theme: default
# random image from a curated Unsplash collection by Anthony
# like them? see https://unsplash.com/collections/94734566/slidev
background: "/assets/img/background.jpg"
# apply any windi css classes to the current slide
class: 'text-center'
# https://sli.dev/custom/highlighters.html
highlighter: shiki
# show line numbers in code blocks
lineNumbers: false
# some information about the slides, markdown enabled
info: |
  gitmars介绍
# persist drawings in exports and build
drawings:
  persist: false
---

# 资管云更新流程梳理

# +git流程工具gitmars培训

<div class="pt-12">
  <span @click="$slidev.nav.next" class="px-2 py-1 rounded cursor-pointer" hover="bg-white bg-opacity-10">
    开始吧 <carbon:arrow-right class="inline"/>
  </span>
</div>

<div class="abs-br m-6 flex gap-2">
  <button @click="$slidev.nav.openInEditor()" title="Open in Editor" class="text-xl icon-btn opacity-50 !border-none !hover:text-white">
    <carbon:edit />
  </button>
  <a href="https://github.com/slidevjs/slidev" target="_blank" alt="GitHub"
    class="text-xl icon-btn opacity-50 !border-none !hover:text-white">
    <carbon-logo-github />
  </a>
</div>

<!--
The last comment block of each slide will be treated as slide notes. It will be visible and editable in Presenter Mode along with the slide. [Read more in the docs](https://sli.dev/guide/syntax.html#notes)
-->

---

<img src="/assets/img/gitmars-branch.png"  />

---

## 为什么要用Gitmars工具


<div style="padding: 0.5em 0;"></div>

### 1. 避免git上的异常操作

<div style="padding: 0.5em 0;"></div>

- 提交到release的内容没有提交到dev
- 在dev上直接修改提交
- 直接合并到master
- 从dev合并到个人分支

<div style="padding: 0.5em 0;"></div>

### 2. 提升代码合并效率

<div style="padding: 0.5em 0;"></div>

- 创建分支

```shell {0|1-4|6-7}
# 一般操作
git checkout release
git pull
git checkout -b feature/test10001

# 使用gitmars
gitm start feature test10001
```

---

- 提交到dev并且构建

```shell {0|1-6|8-9}
# 一般操作
git checkout dev
git pull
git merge feature/test10001
git push
# 跑地铁线

# 使用gitmars
gitm cb -d -b
```

- 创建release合并请求

```shell {0|1-5|7-8}
# 一般操作
git checkout feature/test10001
git merge release
git push
# 提交合并请求

# 使用gitmars
gitm cb -p --description "xxxxxx"
```

---

# 一般任务feature线
一周内要上线的任务
```shell {all|1-2|3-5|6-7|8-9|all}
#同步release并且从release创建新分支feature/xxx_8000
gitm start feature xxx(名字)_8000(任务编码)
#修改代码，注释统一注明任务号和内容
git add .
git commit –m "feature:8000测试"
#同步并且将分支合并到dev
gitm cb -d
#将个人feature线推送到远程分支，并且创建合并请求
gitm cb -p --description "【修改原因】xxxx【修改内容】xxxx"
```


---

# 元数据
```shell {all|1|2-4|5-7|8-11|all}
#开发平台操作后提交、推送到feature/metadata，注释必须完整注明”feature:8000(任务编码)元数据提交”，注释中间不允许空格。
#切换到feature/metadata分支，同步
git checkout feature/metadata
git pull
#切换到feature分支，将内容cherry-pick到feature分支
git checkout feature/xxx_8000
gitm copy -s feature/metadata -k "feature:8000(任务编码)元数据提交" -a xxx(提交人)
#同步并且将分支合并到dev
gitm cb -d
#将个人feature线推送到远程分支，并且创建合并请求
gitm cb -p --description "【修改原因】xxxx【修改内容】xxxx"
```
冲突处理
```shell {all|1|2-3|4-6|all}
#建议每个页面一段时间内只有一个人会进行修改
#执行gitm copy时如果出现冲突，先解决cherry-pick的冲突，解决后执行
gitm continue
#元数据永远只保留最新版本，判断最新版本后，可以使用
git checkout --theirs 冲突文件名
git checkout --ours 冲突文件名
```

---

# 特殊任务feature线
任务持续多周，多人合作
```shell {all|1-9|10-12|all}
#同步release并且从release创建新分支feature/test
gitm start feature test(可以区分的名称)
#修改代码，注释统一注明分支和内容
git add .
git commit –m “feature:test测试”
#同步并且将分支合并到dev
gitm cb -d
#将个人feature线推送到远程分支，并且创建合并请求
gitm cb -p --description "【修改原因】xxxx【修改内容】xxxx"
#每周需要分支负责人在该分支上同步release内容
gitm update
git push
```

<arrow v-click="2" x1="400" y1="420" x2="230" y2="330" color="#564" width="3" arrowSize="1" />


---

# bug线
处理旧版本的bug
```shell {all|1-6|7-11|all}
#同步对应版本号tag拉取分支
gitm start bugfix xxx(名字缩写)_1001(任务编号) --tag 3.0.013.01（tag编码）
#修改代码，注释统一注明分支和内容，推送到远程分支，跑地铁线生成bug修复补丁
git add .
git commit –m “bugfix:test测试”
git push
#如果bug在现在最新版本中也有，需将bug修复合并到dev和release
#同步并且将分支合并到dev
gitm cb -d
#将bugfix分支推送到远程分支，并且创建合并请求
gitm cb -p --as-feature --description "【修改原因】xxxx【修改内容】xxxx"
```


---

# 注意事项

- 禁止在公共分支使用reset，撤回代码请使用revert
- 开发分支(bugfix/feature)合预发环境前必须保证已经合过dev分支
- 超过1周没有上线的分支要及时同步上游分支代码
- commit信息要符合规范，例：git commit -m "feature:80000新增二维码登录方式"
- 分支任务上线后，不要手动删除分支，切到对应分支后，执行gitm end --no-combine可删除分支，在执行前请确保分支已经上线
- 特性分支和标准产品共用metadata分支，容易出现问题，在开发时，需注意兼容性。如差异太多，可以提供独立数据中心。
- 有的任务是在建metadata之前就存在的，上面的元数据合并到其他环境可能会冲突，请手动复制元数据文件。
<p style="font-size:20px;color:red">
- 请所有开发同学进入研发内网gitlab.kingdee.com，右上角设置-Access Token，勾选全部选项，创建个人token后发给蔡梦竹
</p>
<p style="font-size:20px;color:red">
- 为了规范，之后metadata分支变更为feature/metadata，请大家修改一下元数据提交的远程分支名称
</p>


---

# 更多内容
<div style="padding: 0.5em 0;"></div>
<p style="font-size:20px;color:green">
<a href="https://saqqdy.gitee.io/gitmars/" target="_blank" rel="noopener">官方网站</a>
</p>
<p style="font-size:20px;color:green">
<a href="https://saqqdy.gitee.io/gitmars/api/" target="_blank" rel="noopener">指令大全</a>
</p>
<p></p>

-- 对于gitmars的功能有任何建议或意见，请联系蔡梦竹。
