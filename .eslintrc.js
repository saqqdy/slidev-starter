const { simpleTs: config } = require('eslint-config-sets')
const { extend } = require('js-cool')

module.exports = extend(true, config, {
    plugins: ['prettier', 'markdown'],
    rules: {
        'no-unused-vars': 0,
        '@typescript-eslint/no-unused-vars': [
            1,
            {
                argsIgnorePattern: '^h$',
                varsIgnorePattern: '^h$'
            }
        ]
    },
    overrides: [
        {
            // 2. Enable the Markdown processor for all .md files.
            files: ['**/*.md'],
            processor: 'markdown/markdown',
            rules: {
                'MD030/list-marker-space': [
                    1,
                    {
                        ul_single: 3,
                        ol_single: 3,
                        ul_multi: 3,
                        ol_multi: 3
                    }
                ]
            }
        },
        {
            // 3. Optionally, customize the configuration ESLint uses for ```js
            // fenced code blocks inside .md files.
            files: ['**/*.md/*.js'],
            // ...
            rules: {
                // ...
            }
        }
    ]
})
